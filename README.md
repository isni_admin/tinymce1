# tinymce 富文本工具配置使用



### 1. 安装插件

###### 1.1 **安装 ** `@tinymce/tinymce-vue`

Vue 3x 版本 

```shell
npm install --save "@tinymce/tinymce-vue@^4"
```

Vue 2x 版本

```shell
npm install --save "@tinymce/tinymce-vue@^3"
```

###### 1.2 安装 `tinymce`

Vue 2x 版本

```shell
npm install --save "tinymce@5.8.1"
```

###### 1.3 安装 `axios`

```shell
npm install --save axios
```

------



### 2. 配置 

将本项目内 `src/components/tinymce` 目录拷贝到目标项目内的 `components` 之中

将本项目内 `public/tinymce` 目录，拷贝到目标项目内的 `public` 目录中

------



### 3. 使用

```vue
<tinymce  @change="change" upload_url="文件存储接口地址" />
```

文档地址: `https://doc.cms.talelin.com/client/components/tinymce.html`

------



#### 4. 问题说明

这是为了解决 `icons.js` 报错而引入的。

```js
import 'tinymce/icons/default/icons'
```

tinymce 默认是英文的，需要使用语言包汉化， 语言包存放在项目的: `public/tinymce/langs` 之中。



![](F:\wu\Project\Vue\tinymce\README.assets\image-20210730144437687.png)
